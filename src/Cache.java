import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Cache{
	
	private Map<String, ArrayList<SystemEnumeration>>items;
	
	
	private Cache(){
		this.items = new HashMap<String, ArrayList<SystemEnumeration>>();
	}
	
	private static Cache instance;

	public synchronized static Cache getInstance(){
		if (instance == null){
				instance = new Cache();
		}
		return instance;
		
	}
	
	public synchronized void refresh(Updater updater){
		this.clear();
		updater.update();
		items = updater.getUpdatedList();
	}
	
	public ArrayList<SystemEnumeration> get(String enumerationName){
		return items.get(enumerationName);
	}
	
	public synchronized void clear(){
		items.clear();
		
	}

		
}