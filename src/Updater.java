import java.util.ArrayList;
import java.util.Map;


public class Updater implements Runnable{
	
	private int lifespam;
	private Map<String, ArrayList<SystemEnumeration>> c;
	private ArrayList<SystemEnumeration> colors;
	private ArrayList<SystemEnumeration> numbers;
	
	public Updater(int lifespam){
		this.lifespam = lifespam;
	}
	
	public Updater(){
		this.lifespam = 1000;
	}
	
	public void run() {
		while(true){
			try {
				System.out.println("Trying to update cache");
				update();
				Thread.sleep(this.lifespam);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void update(){
		this.colors = getDictionaryColors();
		this.numbers = getDictionaryNumbers();
	
	}
	
	private ArrayList<SystemEnumeration> getDictionaryColors(){

		ArrayList<SystemEnumeration> list = new ArrayList<SystemEnumeration>();
		String name = "Colors";
		SystemEnumeration systeme = new SystemEnumeration();
		systeme.setEnumerationName(name);
		systeme.setValue("black");
		list.add(systeme);
		SystemEnumeration systeme2 = new SystemEnumeration();
		systeme2.setEnumerationName(name);
		systeme2.setValue("white");
		list.add(systeme2);
		
		return list;
	}
	
	private ArrayList<SystemEnumeration> getDictionaryNumbers(){

		ArrayList<SystemEnumeration> list = new ArrayList<SystemEnumeration>();
		String name = "Numbers";
		SystemEnumeration systeme = new SystemEnumeration();
		systeme.setEnumerationName(name);
		systeme.setValue("one" + System.currentTimeMillis());
		list.add(systeme);
		SystemEnumeration systeme2 = new SystemEnumeration();
		systeme2.setEnumerationName(name);
		systeme2.setValue("two" + System.currentTimeMillis());
		list.add(systeme2);
		
		return list;
	}

	public Map<String, ArrayList<SystemEnumeration>> getUpdatedList() {
		c.put("Colors", this.colors);
		c.put("Numbers", this.numbers);
		return c;
		
	}


}
